package epam.com;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)){
            System.out.println("Please input 10 numbers");
            int[] array = new int[10];
            for(int i = 0; i < array.length; i++){
                array[i] = sc.nextInt();
            }
            System.out.println(sortArrayAndReturnAsString(array));
        }catch (Exception e){
            System.out.println(e);
        }
    }

    public static String sortArrayAndReturnAsString(int[] array){
        Arrays.sort(array);
        return Arrays.toString(array);
    }
}