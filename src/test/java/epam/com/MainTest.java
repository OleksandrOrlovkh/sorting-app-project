package epam.com;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class MainTest {

    int[] array;

    public MainTest(int[] array) {
        this.array = array;
    }

    @Parameterized.Parameters()
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{}},
                {new int[]{10}},
                {new int[]{10,9,8,7,6,5,4,3,2,1}},
                {new int[]{100,200,400,500,50,1000,10,9,8,7,6,5,4,3,2,1}}
        });
    }

    @Test
    public void testSorting() {
        assertNotNull(array);
        Arrays.sort(array);
        assertEquals(Arrays.toString(array), Main.sortArrayAndReturnAsString(array));
    }
}